import sys

import requests
from bs4 import BeautifulSoup as bs
import csv
import datetime
import unidecode
import logging
import os

mainUrl = 'https://www.trictrac.net/jeu-de-societe/?order_type=title&order_dir=ASC&page='
delimiterCsv = ";"

entetesCSV = ['Titre', 'Type', 'Nombre de joueurs', 'Age', 'Durée', 'Créateur', 'Illustrateur', 'Editeur', 'Année',
              'Extension', 'Image', 'Note Trictrac']

listeInfosBoite = ["Nb Joueurs", "Age", "Durée"]

workspacePath = "D:\Projets_Python\Scrap"

saveFile = "gamesDatas"
logFile = "scrap.log"

logging.basicConfig(filename=workspacePath + "\\" + datetime.datetime.now().strftime("%d-%m-%Y") + "_" + logFile,
                    level=logging.DEBUG,
                    format='%(asctime)s %(levelname)s [%(funcName)s line %(lineno)d] : %(message)s',
                    datefmt='%H:%M:%S')


def parsePage(link):
    # lxml est un parser
    return bs(requests.get(link, verify=False).text, 'lxml')


def getAllGamesLinksInPage(link):
    return parsePage(link).find_all('a', class_="header")


def getNamesForGame(page):
    infosCasting = {"Créateur": "", "Illustrateur": "", "Editeur": ""}
    try:
        casting = page.find("div", class_="casting").find_all("a")
        for a in casting:
            match a.previous:
                case "Par ":
                    # unidecode est nécessaire pour gérer les accents spéciaux
                    infosCasting["Créateur"] = unidecode.unidecode(a.text)
                case "De ":
                    infosCasting["Créateur"] = unidecode.unidecode(a.text)
                case "Illustré par ":
                    infosCasting["Illustrateur"] = unidecode.unidecode(a.text)
                case "Édité par ":
                    infosCasting["Editeur"] = unidecode.unidecode(a.text)
    except:
        logging.error(datetime.datetime.now().strftime("%X") + ": aucune information casting trouvée")
    finally:
        return infosCasting


def writeToCsv(fileName, gameRows, NbPages):
    with open(fileName, 'w', errors='replace', newline='') as csvfile:
        csv_writer = csv.writer(csvfile, delimiter=";", skipinitialspace=True)
        try:
            csv_writer.writerows(gameRows)
            logging.info("Exporté avec succès " + str(NbPages) + " pages dans le fichier " + fileName)
        except:
            logging.error("Erreur durant l'écriture du fichier CSV")
            logging.error(*sys.exc_info())
            logging.error(gameRows)
            os.rename(fileName, fileName[:(len(fileName) - 4)] + "_WITH-ERROR.csv")


def scrapGamesInfos(mainUrl, startPage, endPage, step=15):
    gameRows = [entetesCSV]
    pagesCount = 0
    for i in range(startPage, endPage):
        logging.info(datetime.datetime.now().strftime("%X") + " : Début du traitement de la Page " + str(i))
        print(datetime.datetime.now().strftime("%X") + " : Début du traitement de la Page " + str(i))
        gamesPagesLinks = getAllGamesLinksInPage(mainUrl + str(i))
        for link in gamesPagesLinks:
            if link['href'] != "#":
                gamePage = parsePage(link['href'])
                gameResume = []
                # Titre
                gameResume.append(gamePage.find("h1").findNext("a").string)
                # Type
                try:
                    gameResume.append(gamePage.find("div", class_="categories").findNext("a").string)
                except:
                    gameResume.append("")
                # Nb joueurs, age, durée
                infosBoite = gamePage.find_all("div", class_="stat mini")
                for info in infosBoite:
                    gameResume.append(info.next)
                # Créateur, illustrateur, éditeur
                for key in getNamesForGame(gamePage):
                    gameResume.append(getNamesForGame(gamePage)[key])
                # année
                try:
                    gameResume.append(gamePage.find("div", class_="part").find_next("h4").find_next("div").string[
                                      -4:])
                except:
                    gameResume.append("")
                # extension
                try:
                    if gamePage.find("a", class_="ribbon red label important").string == "Extension":
                        gameResume.append("Extension")
                except:
                    gameResume.append("")
                # image
                try:
                    gameResume.append(gamePage.find("img", id="img-game")['data-src'].replace(" ", ""))
                except:
                    gameResume.append("no image")
                # note
                try:
                    gameResume.append(gamePage.find("h5", string="Note du jeu").find_next("div", class_="stat").text[
                                      :-3].strip("\n "))
                except:
                    gameResume.append("0.00")

                gameRows.append(gameResume)
        pagesCount += 1
        # découpage des fichiers tous les X pages pour éviter que gamerows soit trop gros et le programme plante
        if pagesCount >= (step):
            csvFileName = workspacePath + "\\" + saveFile + "-" + str(i + 1 - step) + "to" + str(i) + ".csv"
            writeToCsv(csvFileName, gameRows, step)
            gameRows = [entetesCSV]
            pagesCount = 0

    # Il reste des lignes dans gameRows pas encore exportées en csv
    csvFileName = workspacePath + "\\" + saveFile + str(endPage - pagesCount) + "-to" + str(endPage) + ".csv"
    writeToCsv(csvFileName, gameRows, endPage - pagesCount)


scrapGamesInfos(mainUrl, 61, 1000)
