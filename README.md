# TricTrac Scraping


***

## Description
Script python pour faire du webscraping sur le site trictrac.net
Fournit une liste de jeux avec les informations principales extraites du site.

## Installation
Aucune installation nécessaire.
Lancer le script.

## Usage
Permet de récupérer les informations des jeux de sociétés à partir du site trictrac.net
parcours les pages à partir de la liste alphabétique et exporte en CSV toutes les 15 pages pour éviter une trop grosse variable et un plantage.

## Authors and acknowledgment
Fouvet Sébastien.

